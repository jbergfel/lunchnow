from setuptools import setup

setup(name='LunchNow',
      version='1.0',
      description='Application to determine a lunch destination.',
      author='jbergfel',
      author_email='jbergfel@and-how.net',
      url='http://lunchnow.and-how.net/',
      install_requires=['Django==1.4'],
     )
