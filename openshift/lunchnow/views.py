from datetime import datetime,timedelta
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from lunchnow.models import Location, Rating
from lunchnow.forms import RateForm

def home(request):
	# fetch locations unvisited in the past 5 days
	location_list = Location.objects.exclude(last_visited__gt=datetime.today()-timedelta(days=5)).order_by('-score')
	# winner is the location with the highest score
	winner = None
	if location_list.count() > 0:
		winner = location_list[0]
	return render_to_response('lunchnow/home.html',
					{'locations': location_list[1:],
					 'winner': winner},
					context_instance=RequestContext(request))

def rate(request):
	if request.method == 'POST':
		form = RateForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('lunchnow.views.home'))
	else:
		form = RateForm()
	return render_to_response('lunchnow/ratings.html',
					{'form': form},
					context_instance=RequestContext(request))

def visit(request):
	visited = Location.objects.exclude(last_visited__gt=datetime.today()-timedelta(days=5)).order_by('-score')[0]
	visited.last_visited = datetime.today()
	visited.save()
	return HttpResponseRedirect(reverse('lunchnow.views.home'))

