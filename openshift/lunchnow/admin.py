from models import Location, Rating
from django.contrib import admin

admin.site.register(Location)
admin.site.register(Rating)

