from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('lunchnow.views',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'home'),
    url(r'^rate$', 'rate'),
    url(r'^visit$', 'visit'),
)
