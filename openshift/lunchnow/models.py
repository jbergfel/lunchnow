from django.db import models

class Location(models.Model):
	name = models.CharField(max_length=32)
	score = models.FloatField(default=0.0)
	created = models.DateTimeField(auto_now_add=True)
	last_visited = models.DateTimeField(blank=True,null=True)
	def __unicode__(self):
		return self.name

class Rating(models.Model):
	RATING_CHOICES = (
		(5, 'Love it'),
		(3, 'meh'),
		(1, 'Hate it'),
	)
	location = models.ForeignKey(Location)
	rating = models.IntegerField(choices=RATING_CHOICES)
	def __unicode__(self):
		return self.location.name
	def save(self, *args, **kwargs):
		super(Rating, self).save(*args, **kwargs)
		self.location.score += self.rating
		self.location.save()

